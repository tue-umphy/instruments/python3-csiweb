
Installation
============

:mod:`csiweb` is best installed via :mod:`pip` from the repository root:

.. code-block:: sh

    python3 -m pip install --user .

Depending on your setup it might be necessary to install the :mod:`pip` module
first:

.. code-block:: sh

    # Debian/Ubuntu
    sudo apt-get install python3-pip

Or see `Installing PIP`_.

.. _Installing PIP: https://pip.pypa.io/en/stable/installing/
