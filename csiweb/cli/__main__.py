# system modules

# internal modules
from csiweb.cli.commands.main import cli

# external modules


if __name__ == "__main__":
    cli(prog_name="python3 -m csiweb.cli")
