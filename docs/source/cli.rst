Command-Line Interface
======================

:mod:`csiweb` comes with a :doc:`cli`.

Invocation
++++++++++

:mod:`csiweb`'s :doc:`cli` is invoked from the command-line by launching the
``csiweb`` command:

.. code-block:: sh

    csiweb
    # Usage: csiweb [OPTIONS] COMMAND [ARGS]...
    #
    #  Campbell Scientific CSI Web Server client
    # ...

If running the above fails with something sounding like ``command not found:
csiweb``, you may either always run ``python3 -m csiweb`` instead of a
plain ``csiweb`` or add the directory ``~/.local/bin`` to your ``PATH``
environment variable by appending the following line to your shell's
configuration (probabily ``.bashrc`` in your home directory):

.. code-block:: sh

   export PATH="$HOME/.local/bin:$PATH"

The :doc:`cli` is based on `Click`_ and requires a subcommand to be specified.
The available subcommands will be explained throughout this page.

Help
----

A help page is available for both the ``csiweb`` command and each
subcommand. To access it, pass the ``--help`` (or ``-h``) option to it:

.. code-block:: sh

    csiweb --help
    # Usage: csiweb [OPTIONS] COMMAND [ARGS]...
    #
    #  Campbell Scientific CSI Web Server client
    # ...
    csiweb --simulate query --help
    # Usage: csiweb query [OPTIONS] COMMAND [ARGS]...
    #
    #  Poll data from the CSI web server
    # ...




.. _cli logging:

Logging
-------

If something is not working as expected or the :doc:`cli` seems to ”hang”, you
might want to check some more verbose logging output to see what's going on.
You can increase the verbosity of the :doc:`cli` by passing the ``--verbose``
(or ``-v``) option one or multiple times to the ``csiweb`` command:

.. code-block:: sh

   csiweb -v ...
   # a little more output
   # ...
   csiweb -vvvv ...
   # VERY much logging output
   # ...
   CSIWEB_VERBOSITY=4 csiweb
   # same as the above four -v

Analogously, if you'd like less verbosity, specify the ``--quiet`` (or ``-q``
or the ``CSIWEB_QUIETNESS`` environment variable)
options one or more times.

.. hint::

   Keep in mind that you have to specify the logging options **to the**
   ``csiweb`` **command, BEFORE any subcommand**!

.. _cli client setup:

Setting up the Client
+++++++++++++++++++++

.. note::

   The :doc:`cli` is structured as a chain of commands to set up specific
   actions so that a one-liner is enough.

The ``csiweb`` main command is also used to set up the connection to the data
logger. By default, the first default gateway address that can be found for a
wireless interface is used as the logger's IP address. If that fails, the IP
address has to be specified. For testing purposes, the ``--simulate`` option
can be used:

.. code-block:: sh

   csiweb ...
   # uses the first found default gateway of a wireless interface
   csiweb --ip 192.168.67.10 ...
   # specified IP
   csiweb --simulate
   # fakes responses from the logger (very limited, for testing only)

.. _cli query:

Querying data
+++++++++++++

”After” setting up the connection, the client can ``query`` data from the data
logger:

.. code-block:: sh

   csiweb query
   # one-shot data query and printout
   csiweb query --interval 10
   # query data every 10 seconds

.. _cli publish:

Publishing Data via MQTT
++++++++++++++++++++++++

”After” setting up the data query, the client can ``publish`` retrieved data
to one or more `MQTT`_ brokers. Internally, the `paho-mqtt`_-module is used for
that.

.. code-block:: sh

   csiweb query -i 10 publish \
        -b "localhost/datalogger/{quantity}?only=T_probe&T_probe=temperature"
        -b "123.3.1.2:1883/datalogger/{quantity}?interval=20&only=T_probe&T_probe=temperature"
   # This will query data from the logger every 10 seconds, publish data to
   # datalogger/temperature to an MQTT broker on localhost but only every 20
   # seconds to an MQTT broker on 123.3.1.2:1883.


.. _paho-mqtt: http://pypi.python.org/pypi/paho-mqtt
.. _MQTT: https://en.wikipedia.org/wiki/MQTT
.. _Click: https://click.palletsprojects.com
.. _Mosquitto: https://mosquitto.org
.. _Systemd User Service Unit: https://wiki.archlinux.org/index.php/Systemd/User


